rm -rf /opt/ANDRAX/termineter

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/pip3 install wheel setuptools

/opt/ANDRAX/PYENV/python3/bin/pip3 install .

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
